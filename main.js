export function setup(ctx) {
    const id = 'semi-auto-switch-gear-set';
    const title = 'SEMI Auto Switch Gear Set';

    const SETTING_GENERAL = 'General'

    ctx.settings.section(SETTING_GENERAL).add({
        'type':'switch',
        'name':`${id}-enable`,
        'label':`Enable ${title}`,
        'default':true
    })

    const enemyStyle = () => game.combat.enemy.attackType;

    const getSetAttackTypes = () => {
        return game.combat.player.equipmentSets.map((x) => x.equipment.slots['Weapon']?.item?.attackType)
    };

    const getWantedSet = () => {
        const currentSets = getSetAttackTypes();

        if (enemyStyle() === 'magic') {
            return currentSets.indexOf('ranged');
        }
        if (enemyStyle() === 'ranged') {
            return currentSets.indexOf('melee');
        }
        if (enemyStyle() === 'melee') {
            return currentSets.indexOf('magic');
        }
    };

    ctx.patch(CombatManager, 'spawnEnemy').after((result) => {
        const autoGearSetSwapEnabled = ctx.settings.section(SETTING_GENERAL).get(`${id}-enable`)

        // Is this script enabled?
        if(!autoGearSetSwapEnabled) {
            mod.api.SEMI.log(id, 'Mod is disabled.')
            return;
        }

        // Find the set we want
        const wantedSetIndex = getWantedSet();

        // If we don't have a valid set, exit
        if (wantedSetIndex < 0) {
            mod.api.SEMI.log(id, 'WARNING: You do not have a valid equipment set. Running from combat.');
            game.combat.stop()
            return;
        }

        // If we have the right set, stay put
        if (wantedSetIndex === game.combat.player.selectedEquipmentSet) {
            return;
        }
        
        game.combat.player.changeEquipmentSet(wantedSetIndex);
    })

    mod.api.SEMI.log(id, 'Mod loaded successfully')
}
